window.jQuery = $ = require('jquery');
window.lazyload = require('lazysizes');
window.aos = require('aos');
window.Slick = require('slick-carousel');
window.magnificPopup = require('magnific-popup');

