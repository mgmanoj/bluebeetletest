var props = require('../properties.js');
var path = require('path');
var gulp = require('gulp');
var cleanCSS = require('gulp-clean-css');

gulp.task('minify-css', function() {
    var isAr = props.lang == 'ar';
    var stylesLocation = isAr ? 'app/ar/styles/*.css': 'app/en/styles/*.css';
    return gulp.src(stylesLocation)
        .pipe(cleanCSS({debug: true}, function(details) {
            console.log(details.name + ': ' + details.stats.originalSize);
            console.log(details.name + ': ' + details.stats.minifiedSize);
        }))
        .pipe(gulp.dest(props.outFolder.styles));
});
