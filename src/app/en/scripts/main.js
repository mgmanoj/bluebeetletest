/* global $ */
require('bootstrap-sass/assets/javascripts/bootstrap.js');
require('svgxuse');

var header = require('header');
var generic = require('generic');
var gallery = require('gallery');

jQuery(function () {

	// Slick slider
	jQuery('.carousel').slick();

});

gallery();
header();
generic();


jQuery(window).on('load',function() {
	
	jQuery('html').removeClass('loading');
	//AOS
	aos.init({disable: 'mobile'});

});
