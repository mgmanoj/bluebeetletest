module.exports = function(){
	// Header related scripts

	var winH = jQuery(window).height();
	jQuery(window).on('scroll',function(){
		var sticky = jQuery('.site-header'),
	    scroll = jQuery(window).scrollTop();

		if (scroll >= winH) {sticky.addClass('is-sticky')}
		else {sticky.removeClass('is-sticky')};
	});
}