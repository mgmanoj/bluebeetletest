module.exports = function () {
  // cover image
  jQuery('[data-coverimg]').each(function() {
    var sourceImg = jQuery(this).data('coverimg');
    if (sourceImg.match("^#")) {
      jQuery(this).css('background-color', sourceImg);
    }
    else{
      jQuery(this).css('background-image', 'url(' + sourceImg + ')');
    }
  });

  //form validation

  jQuery(".form--newsletter").on("submit", function(e){  

    var valid = true,
        message = '';
    
    jQuery('.form--newsletter input').each(function() {
      var $this = jQuery(this);
      
      if(!$this.val()) {
          var inputName = $this.attr('name');
          valid = false;
          message += 'Please enter your ' + inputName + '\n';
      }
      if(!valid) {
        jQuery(this).addClass('error');
        jQuery(this).parent().find('.error__message').show();
        e.preventDefault();
      }
      else {
        jQuery(this).removeClass('error');
        jQuery(this).parent().find('.error__message').hide();

      }

    });
    
    return valid;
  });

};