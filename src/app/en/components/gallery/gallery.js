module.exports = function(){
  // Magnific popup image/ video gallery

  jQuery('.gallery__list').each(function() {
    var item = jQuery(this).find('.gallery__item');

    var items = [];
    jQuery(item).each(function() {
      var item = jQuery(this);
      var type = 'image';
      if (jQuery(item).hasClass('gallery__video')) {
        type = 'iframe';
      }
      var magItem = {
        src: jQuery(item).data('src'),
        type: type
      };
      magItem.title = jQuery(item).data('title');
        items.push(magItem);
      });

    jQuery(item).magnificPopup({
      mainClass: 'mfp-fade',
      items: items,
      gallery:{
          enabled:true,
          tPrev: jQuery(this).data('prev-text'),
          tNext: jQuery(this).data('next-text')
      },
      type: 'image',
      callbacks: {
        beforeOpen: function() {
          var index = jQuery(item).index(this.st.el);
          if (-1 !== index) {
            this.goTo(index);
          }
        }
      }
    });
  });

}